package ir.alirezaiyan

import io.ktor.application.*
import io.ktor.response.*
import io.ktor.request.*
import io.ktor.routing.*
import io.ktor.http.*
import io.ktor.html.*
import kotlinx.html.*
import kotlinx.css.*
import io.ktor.content.*
import io.ktor.http.content.*
import io.ktor.client.*
import io.ktor.client.engine.apache.*

import io.ktor.application.*
import io.ktor.html.*
import io.ktor.http.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import kotlinx.css.*
import kotlinx.css.properties.*
import kotlinx.html.*

const val username = "Ali Rezaiyan"
const val email = "alirezaiyann@gmail.com"
const val github = "https://github.com/rezaiyan"
const val aboutMe = "I'm an android developer with more than 4 years of experience." +
        " I worked with the companies which they have major users and it makes me involved with" +
        " various challenges and I learned lots of things."

val primaryColor = Color("#ff7c7c")
val titleColor = Color("#21243d")
val accentColor = Color("#ffd082")
val subtitleColor = Color("#88e1f2")

fun main(args: Array<String>): Unit = EngineMain.main(args)

@Suppress("unused") // Referenced in application.conf
fun Application.module() {

    routing {
        get("/") {
            call.respondHtml {
                head { link(rel = "stylesheet", href = "/styles.css", type = "text/css") }
                body {
                    Color.black
                    p ("username"){+username}
                    p ("email"){ a(href = "mailto:$email"){+email} }
                    p ("github"){ a(href = github, target = "_blank"){+"Visit my GitHub repositories"} }
                    div ("aboutMe"){
                        p ("aboutMe") {+aboutMe}
                    }
                }
            }
        }

        get("/styles.css") {
            call.respondCss {
                body {
                    backgroundColor = primaryColor
                    textAlign = TextAlign.center
                    alignContent = Align.center
                }
                p {
                    color = titleColor
                    fontSize = 2.em
                }

                rule("div.aboutMe"){
                    alignSelf = Align.center
                    width = LinearDimension("50%")
                    margin(LinearDimension.auto)
                }

                rule("p.email"){
                    fontSize = 1.em
                    marginTop = (-1.5).em
                }
            }
        }
    }
}

fun FlowOrMetaDataContent.styleCss(builder: CSSBuilder.() -> Unit) {
    style(type = ContentType.Text.CSS.toString()) {
        +CSSBuilder().apply(builder).toString()
    }
}

fun CommonAttributeGroupFacade.style(builder: CSSBuilder.() -> Unit) {
    this.style = CSSBuilder().apply(builder).toString().trim()
}

suspend inline fun ApplicationCall.respondCss(builder: CSSBuilder.() -> Unit) {
    this.respondText(CSSBuilder().apply(builder).toString(), ContentType.Text.CSS)
}